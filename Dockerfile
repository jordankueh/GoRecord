FROM golang:1.10
WORKDIR /go/src/app
RUN go get -d -v github.com/aws/aws-sdk-go
COPY . .
RUN go build -v -x -o gorecord gorecord.go
ENTRYPOINT "./gorecord"
